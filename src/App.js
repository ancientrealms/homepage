import {Button, ButtonGroup, Col, Container, Image, Row} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'

function App() {
    return (
        <Container className="max-height">
            <Row className="head-spacer"/>
            <Row className="main-row">
                <Col/>
                <Col className="d-flex justify-content-center text-center main-column">
                    <Row>
                        <Col md={3}/>
                        <Col md={6}><Image className="logo" src="logo.png"/></Col>
                        <Col md={3}/>
                    </Row>
                    <Row className="nav-bar-row">
                        <ButtonGroup size="lg" className="mb-2 navbar">
                            <Button className="navbar-element" href="https://forum.ancientrealms.it">Forum</Button>
                            <Button className="navbar-element disabled" href="https://wiki.ancientrealms.it">Wiki</Button>
                            <Button className="navbar-element disabled" href="/">Votaci</Button>
                            <Button className="navbar-element" href="https://www.patreon.com/AncientRealms">Supportaci</Button>
                        </ButtonGroup>
                    </Row>
                    <Row className="row-spacer"></Row>
                    <Row>
                        <a href="https://www.instagram.com/ancientrealms.it/" className="fab fa-instagram"></a>
                        <a href="https://t.me/joinchat/45gCd90LRj45M2M8" className="fab fa-telegram"></a>
                        <a href="https://steamcommunity.com/groups/AncientRealms" className="fab fa-steam"></a>
                        <a href="https://discord.gg/cYzx3NK3cB" className="fab fa-discord"></a>
                        <a href="https://www.youtube.com/channel/UC8LJBKuwzfroDRYO1xnWs7g" className="fab fa-youtube"></a>
                    </Row>
                    <Row>
                        <a href="https://forum.ancientrealms.it/help/terms/" className="terms" target="_blank">Termini e Condizioni</a>
                    </Row>
                </Col>
                <Col/>
            </Row>
        </Container>
    );
}

export default App;
